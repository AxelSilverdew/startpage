import { createApp } from 'vue'
import App from './App.vue'
import './index.css';

import BookmarksGrid from './components/BookmarksGrid.vue';
import Bookmark from './components/Bookmark.vue';
import CenterModal from './components/CenterModal.vue';

let app = createApp(App);

app.component('bookmarks-grid', BookmarksGrid);
app.component('bookmark', Bookmark);
app.component('center-modal', CenterModal);

app.mount('#app');
